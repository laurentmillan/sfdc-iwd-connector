/***
  Laurent Millan <laurent.millan@genesys.com>

  Listen for events from Salesforce and automatically :
   - creates a Task in IWD based on the object created in salesforce and a mapping table.

  In this mode the Task will carry an attached data that point toward this objects
  to allow screenpop. The agent doesn't have to use SFDC Omnichanel in this mode.

***/

const soap = require('soap');
const template = require('../libs/template.js');

const deepFind = function(obj, path) {
  var paths = path.split('.')
    , current = obj
    , i;

  for (i = 0; i < paths.length; ++i) {
    if (current[paths[i]] == undefined) {
      return undefined;
    } else {
      current = current[paths[i]];
    }
  }
  return current;
}

module.exports = class Handler{
  constructor(config){
    this.org = config.org;
    this.oauth = config.oauth;
    this.topic = config.topic;
  }

  handleEventCreated(sObject){
    // Look for this Task
    return this.getTask(sObject.Id).then(task => {
      console.log(`*** Task already created for id ${sObject.Id} ***`);
    }).catch(err => {
      console.log(`** Task to create for id ${sObject.Id} **`);

      /*** Create the task ***/
      this.generateTask(sObject.Id)
      .then(taskToCreate => {
        console.log("Task to create : ", JSON.stringify(taskToCreate, null, 2));
        if(this.topic.enabled)
          return this.createTask(taskToCreate);
        else{
          console.log(`*** Task ${sObject.id} not created (topic not enabled in configuration) ***`);
        }
      })
      .then( task => {
        if(task)
          console.log(`*** Task created for id ${sObject.Id} ***`);
      })
      .catch( err => {
        console.log(err);
      })

    })
  }

  handleEventUpdated(sObject){
    this.generateTask(sObject.Id).then( task => {
      console.log(JSON.stringify(task, null, 2));
    })
    .catch( err => {
      console.log("CREATE TASK FAIL");
      console.log(err);
    })
  }

  getSobjectDetails(sObjectId, q){
    return new Promise((resolve, reject) => {
      if(!sObjectId){
        reject(`sObject.Id EMPTY`)
      }else{
        if(!q) q = `SELECT Id, CaseNumber, Reason, Type, Subject, Status, CreatedDate, DueDate__c, SupportCategory__c, Priority, Team__c, Origin, Team__r.CTI_Skill__c, Contact.id, Contact.name, contact.email, contact.homephone, contact.mobilephone,contact.WorkPhone__c FROM Case WHERE Id='${sObjectId}'`;

        this.org.query({ query: q }, function(err, resp){
          if(!err && resp.records) {
            resolve(resp.records[0]);
          }else{
            reject(err);
          }
        });
      }
    });
  }

  getTask(captureId){
    return new Promise((resolve, reject) => {
      this.client.getTaskByCaptureId({captureId: captureId}, (err, result) => {
        if(err){
          reject(err);
        }else{
          resolve(result);
        }
      })
    });
  }

  createTask(args){
    return new Promise((resolve, reject) => {
      this.client.createTask(args, (err, result) => {
        console.log(result);
        if(err){
          reject(err);
        }else{
          resolve(result);
        }
      })
    });
  }

  /*
    Generate a Task based on a given sObject Id and the connector configuration
  */
  generateTask(sObjectId){
    return new Promise((resolve, reject) => {

      let sObjectToQuery = this.topic.sfdc.sObjectName;
      let query = `SELECT ` + this.topic.taskSetup.sfdcFieldsToGetFromSobject.join(', ') + ` FROM ${sObjectToQuery} WHERE Id='${sObjectId}'`;

      this.getSobjectDetails(sObjectId, query)
      .then( sObjectDetails => {
        console.log("sObjects Details : ", JSON.stringify(sObjectDetails, null, 2));
        let newTask = {};

        // The content of the sobject field configured to represent priority
        let priorityInfo = sObjectDetails._fields[this.topic.prioritySetup.sObjectFields];
        // The value of this priority field
        let priorityValue = this.topic.prioritySetup.priorityMap[priorityInfo];
        // The priority based on priority map
        let priority = priorityValue?priorityValue:this.topic.prioritySetup.defaultPriority;
        newTask.priority = priority;
        newTask.businessValue = priority;

        // Parse properties
        Object.keys(this.topic.taskSetup.propertiesMap).forEach(propName => {
          let value = this.topic.taskSetup.propertiesMap[propName];
          // If value starts with a sobject. => find the value in the sobject
          if(value == null)
            newTask[propName] = null;
          else{
            try{
              newTask[propName] = template(value, JSON.parse(JSON.stringify(sObjectDetails)));
            }catch(err){}
          }
        })

        // Parse ext
        if(this.topic.taskSetup.extMap) newTask.ext = {};
        Object.keys(this.topic.taskSetup.extMap).forEach(propName => {
          let value = this.topic.taskSetup.extMap[propName];
          // If value starts with a sObject. => find the value in the sObject object
          if(value == null)
            newTask.ext[propName] = null;
          else{
            try{
              newTask.ext[propName] = template(value, JSON.parse(JSON.stringify(sObjectDetails)));
            }catch(err){}
          }
        })

        // Parse data
        if(this.topic.taskSetup.dataMap) newTask.data = { entry: []};
        Object.keys(this.topic.taskSetup.dataMap).forEach(propName => {
          let value = this.topic.taskSetup.dataMap[propName];
          // If value starts with a sObject. => find the value in the sObject object
          if(value == null)
            newTask[propName] = null;
          else{
            let val = "";
            try{
              val = template(value, JSON.parse(JSON.stringify(sObjectDetails)));
              newTask.data.entry.push({
                key: propName,
                value: val
              });
            }catch(err){}
          }
        })

        resolve(newTask);
      })
      .catch( err => console.log(err));
    });
  }

  handleEventDeleted(sObject, event){
    console.log(`* Object ${sObject.Id} deleted *`);
  }

  handle(event){
    let sObject = event.sobject;
    console.log("sObjectFomr event", JSON.stringify(sObject, null, 2));
    /*
    { Status: 'In Progress',
     Origin: 'Email',
     Type: null,
     SupportCategory__c: 'Order Inquiry',
     Priority: 'Normal',
     CreatedDate: '2018-04-03T12:20:22.000+0000',
     Team__c: null,
     CaseNumber: '46376516',
     Id: '500q000000AKUTdAAP',
     Reason: null,
     Subject: 'Demo 10',
     DueDate__c: '2018-04-03T12:20:00.000+0000' }
    */

    switch(event.event.type){
      case "created":
        this.handleEventCreated(sObject, event);
        break;
      case "updated":
        this.handleEventUpdated(sObject, event);
        break;
      case "deleted":
        this.handleEventDeleted(sObject, event);
        break;
    }
  }

  init(){
    return new Promise((resolve, reject) => {
      soap.createClient(this.topic.genesys.capturePoint.url, (err, client) => {
        if(err)
          reject(err)
        else{
          this.client = client;
          resolve(client);
        }
      })
    });
  }

}
