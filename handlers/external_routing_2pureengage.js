/***
  Laurent Millan <laurent.millan@genesys.com>

  Listen for events from Salesforce and automatically :
   - creates a Task in IWD based on the object created in salesforce and a mapping table.

  Events are PendingServiceRouting : each event is a chat session created by
  salesforce and will be handled by the agent using sfdc omnichanel desktop. .
  When the agent will receive the IWD Task in his destop a script must be run to
  create an AgentWork object in salesforce in order to make this chat visible in
  the agent's salesforce omnichanel desktop.

***/
const soap = require('soap');
const template = require('../libs/template.js');

module.exports = class Handler{
  constructor(config){
    this.oauth = config.oauth;
    this.topic = config.topic;
  }

  init(){
    return new Promise((resolve, reject) => {
      soap.createClient(this.topic.genesys.capturePoint.url, (err, client) => {
        if(err)
          reject(err)
        else{
          this.client = client;
          resolve(client);
        }
      })
    });
  }

  /*
    Generate a Task based on a given sObject Id and the connector configuration
  */
  generateTask(sObject){
    return new Promise((resolve, reject) => {
      let newTask = {};

      // The priority based on priority map
      newTask.priority = this.topic.prioritySetup.defaultPriority;
      newTask.businessValue = this.topic.prioritySetup.defaultPriority;

      // Parse properties
      Object.keys(this.topic.taskSetup.propertiesMap).forEach(propName => {
        let value = this.topic.taskSetup.propertiesMap[propName];
        // If value starts with a sobject. => find the value in the sobject
        if(value == null)
          newTask[propName] = null;
        else{
          try{
            newTask[propName] = template(value, JSON.parse(JSON.stringify(sObject)));
          }catch(err){}
        }
      })
      //console.log(newTask);

      // Parse ext
      if(this.topic.taskSetup.extMap) newTask.ext = {};
      Object.keys(this.topic.taskSetup.extMap).forEach(propName => {
        let value = this.topic.taskSetup.extMap[propName];
        // If value starts with a sObject. => find the value in the sObject object
        if(value == null)
          newTask.ext[propName] = null;
        else{
          try{
            newTask.ext[propName] = template(value, JSON.parse(JSON.stringify(sObject)));
          }catch(err){}
        }
      })
      //console.log(newTask);

      // Parse data
      if(this.topic.taskSetup.dataMap) newTask.data = { entry: []};
      Object.keys(this.topic.taskSetup.dataMap).forEach(propName => {
        let value = this.topic.taskSetup.dataMap[propName];
        // If value starts with a sObject. => find the value in the sObject object
        if(value == null)
          newTask[propName] = null;
        else{
          let val = "";
          try{
            val = template(value, JSON.parse(JSON.stringify(sObject)));
            newTask.data.entry.push({
              key: propName,
              value: val
            });
          }catch(err){}
        }
      })

      //console.log(newTask);
      resolve(newTask);
    })
  }

  handleEventCreated(sObject, event){
    /*let taskToCreate = {
      "captureId": sObject.Id,
      "channel": "LiveAgent",
      "category": "LiveAgent",
      "mediaType": "LiveAgent",
      "createdDateTime": sObject.CreatedDate,
      "activationDateTime": sObject.CreatedDate,
      "dueDateTime": null,
      "expirationDateTime": null,
      "ext": {
        "customerId": null,
        "productType": null,
        "sourceProcessType": "SALESFORCE",
        "sourceTenant": "Environment"
      },
      "data":{
        entry: [
          { key: "Subject", value: "LiveAgent" },
          { key: "sfdc_Id", value: sObject.Id },
          { key: "sfdc_ServiceChannelId", value: sObject.ServiceChannelId },
          { key: "sfdc_WorkItemId", value: sObject.WorkItemId },
          { key: "sfdc_QueueId", value: sObject.QueueId },
          { key: "MediaType", value: "liveagentchat" },
          { key: "id_transfer_object", value: `/${sObject.Id}`}
        ]
      }
    }
    */

    /*** Create the task ***/
    this.generateTask(sObject)
    .then(taskToCreate => {
      console.log("Task to create : ", JSON.stringify(taskToCreate, null, 2));

      if(this.topic.enabled){
        this.createTask(taskToCreate)
        .then( task => {
          console.log(`*** Task created for id ${sObject.Id} ***`);
        })
        .catch( err => {
          console.log(err);
        })
      }
      else{
        console.log(`*** Task ${sObject.id} not created (topic not enabled in configuration) ***`);
      }
    })
  }

  createTask(args){
    return new Promise((resolve, reject) => {
      this.client.createTask(args, (err, result) => {
        console.log(result);
        if(err){
          reject(err);
        }else{
          resolve(result);
        }
      })
    });
  }

  handle(event){
    let sObject = event.sobject;
    console.log("sObjectFomr event", JSON.stringify(sObject, null, 2));

    switch(event.event.type){
      case "created":
        this.handleEventCreated(sObject, event);
        break;
      case "updated":
        //this.handleEventUpdated(sObject, event);
        break;
      case "deleted":
        //this.handleEventDeleted(sObject, event);
        break;
    }
  }
}
