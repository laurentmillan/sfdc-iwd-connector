const nforce  = require('nforce');
const http    = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const fs      = require('fs');
let config;
let org;

const app     = express();

const saveConfig = function(){
  fs.writeFile("config.json", JSON.stringify(config, null, 2), function(err) {
      if(err)
        return console.log(err);
      console.log("Data saved");
  });
}
const loadData = function(callback){
  fs.readFile("config.json", "utf8", (err, readData) => {
    if(err)
      return console.log(err);
    else
      config = JSON.parse(readData);

    callback();
    console.log("Data loaded");
  });
}
const backupConfig = function(){
  let date = (new Date()).toISOString().replace(/[:\.-]/gi,"").substring(0,15);
  fs.writeFile(`config_${date}.json`, JSON.stringify(config, null, 2), function(err) {
      if(err)
        return console.log(err);
      console.log("Data saved");
  });
}

const authChecker = function(req, res, next){
  console.log(req.method + " " + req.originalUrl + " " + req.get('Authorization'));
  let allowedPath = ["/login", "/fakeTask"];
  if (allowedPath.indexOf(req.path) == -1) {
    let AuthToken = req.get('Authorization');
    if(config.authTokens.find(validTokens => validTokens == AuthToken)){
      next();
    }else{
      if(req.method == "OPTIONS")
        next();
      else
        res.status(401).end();
    }
  } else{
    next();
  }
}
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Allow-Methods", "GET, PUT, PATCH, POST, DELETE");
  next();
});
app.use(authChecker);

const init = function(){
  org = nforce.createConnection({
    clientId: config.sfdc.clientId,
    clientSecret: config.sfdc.clientSecret,
    redirectUri: 'http://localhost:3000/oauth/_callback',
    //apiVersion: 'v36.0',  // optional, defaults to current salesforce API version
    //environment: 'production',  // optional, salesforce 'sandbox' or 'production', production default
    environment: config.sfdc.environment,  // optional, salesforce 'sandbox' or 'production', production default
    mode: 'single' // optional, 'single' or 'multi' user mode, multi default
  });

  // Init salesforce Streaming API
  org.authenticate({ username: config.sfdc.username, password: config.sfdc.password, securityToken: config.sfdc.securityToken }, (err, oauth) => {
    if(err) return console.log(err);
    if(!err) {
      console.log('*** Successfully connected to Salesforce ***');
    }

    app.get('/sfdc/sobjects/:sObjectName/describe', function(req, res) {
      org.getDescribe(req.params.sObjectName, function(err, data){
        if(!data)
          res.status(403).end();
        else
          res.send(data.fields.map(sObjectField => {
            return {
              name: sObjectField.name.toLowerCase(),
              label: sObjectField.label.toLowerCase(),
              picklistValues: sObjectField.picklistValues
            }
          }))
      })
    })

    app.get('/sfdc/sobjects', function(req, res) {
      org.getSObjects(function(err, data){
        res.send(data.sobjects.map(sObject => {
          return{
            name: sObject.name,
            label: sObject.label
          }
        }))
      })
    })

    app.get('/sfdc/pushtopics', function(req, res) {
      let query = `SELECT ApiVersion,Description,Id,Name,Query FROM PushTopic`;

      org.query({ query: query }, function(err, resp){
        if(!err && resp.records) {
          res.send(resp.records);
        }else{
          res.status(500).send(err);
        }
      });
    })

    // Subscribe to all topics
    config.topics.forEach(topic => {
      const Handler = require(`./handlers/${topic.handler}.js`);
      let cpHandler = new Handler({
          org: org,
          oauth: oauth,
          topic: topic
      });

      cpHandler.init().then(client => {
        console.log(`*** SOAP CapturePoint connection is active for topic ${topic.sfdc.push_topic} ***`);
      })
      .catch(err => {
        console.log(`!!! No way to connect to the Capture Point ${topic.genesys.capturePoint.url}. Don't forget the ?wsdl at the end of the url. !!!`);
      })

      let stream = org.stream({ topic: topic.sfdc.push_topic, oauth: oauth });

      stream.on('connect', function(){
        console.log('Connected to pushtopic: ' + topic.sfdc.push_topic + ` [enabled: ${topic.enabled}]`);
      });

      stream.on('error', function(error) {
        console.log('Error received from pushtopic: ' + error);
      });

      stream.on('data', function(data) {
        console.log(`\n\nReceived the following from pushtopic /topic/${topic.sfdc.push_topic}`);
        console.log(data);
        cpHandler.handle(data);
      });
    })
  });

  app.get('/config', function(req, res) {
    res.send(config);
  })

  /*
  app.post('/config', function(req, res) {
    config = req.body;

    saveConfig();
    res.send(config);
  })
  */

  app.get('/topics', function(req, res) {
    res.send(config.topics);
  })

  app.get('/topics/:name', function(req, res) {
    let topic = config.topics.find(
      topic => topic.name.toLowerCase() == req.params.name.toLowerCase());

    if(topic)
      res.send(topic);
    else
      res.status(404).send("Topic not found");
  })

  app.put('/topics/:name', function(req, res) {
    let topic = config.topics.find(
      topic => topic.name.toLowerCase() == req.params.name.toLowerCase());

    config.topics.splice(config.topics.indexOf(topic), 1, req.body);

    if(topic){
      res.send(req.body);
      saveConfig();
    }
    else
      res.status(404).send("Topic not found");
  })

  app.post('/topics', function(req, res) {
    let topic = config.topics.find(
      topic => topic.name.toLowerCase() == req.body.name.toLowerCase());

    let defaultTopicConfig = {
      "name": req.body.name,
      "handler": req.body.handler,
      "sfdc": {
        "push_topic": req.body.sfdc.push_topic,
        "sObjectName": ""
      },
      "genesys": {
        "capturePoint": {
          "url": ""
        }
      },
      "prioritySetup": {
        "sObjectField": "",
        "priorityMap": {},
        "defaultPriority": 100
      },
      "taskSetup": {
        "sfdcFieldsToGetFromSobject": [],
        "propertiesMap": {
          "captureId": "",
          "channel": "",
          "category": null,
          "mediaType": "",
          "createdDateTime": "",
          "activationDateTime": "",
          "dueDateTime": "",
          "expirationDateTime": "",
          "processId": null
        },
        "extMap": {
          "customerId": "",
          "productType": "",
          "sourceProcessType": "SALESFORCE",
          "sourceTenant": "Environment"
        },
        "dataMap": {
          "MediaType": "workitem"
        }
      }
    }


    if(topic)
      res.status(403).send("Topic existing");
    else{
      config.topics.push(req.body);
      saveConfig();
      res.send(req.body);
    }

    config.topics.splice(config.topics.indexOf(topic), 1, req.body);
  })

  app.post('/backup', function(req, res){
    backupConfig();
    res.status(200).end();
  });

  const server = http.createServer(app).listen(config.serverPort, function () {
    console.log("*** API Started ***");
  });
}

/*
var org = nforce.createConnection({
  clientId: 'SOME_OAUTH_CLIENT_ID',
  clientSecret: 'SOME_OAUTH_CLIENT_SECRET',
  redirectUri: 'http://localhost:3000/oauth/_callback',
  apiVersion: 'v27.0',  // optional, defaults to current salesforce API version
  environment: 'production',  // optional, salesforce 'sandbox' or 'production', production default
  mode: 'multi' // optional, 'single' or 'multi' user mode, multi default
});



org.authenticate({ username: config.username, password: config.password, securityToken: config.securityToken }, (err, oauth) => {
  if(err) return console.log(err);
  if(!err) {
    console.log('*** Successfully connected to Salesforce ***');
    // add any logic to perform after login
  }

  // subscribe to a pushtopic
  var str = org.stream({ topic: config.PUSH_TOPIC, oauth: oauth });

  str.on('connect', function(){
    console.log('Connected to pushtopic: ' + config.PUSH_TOPIC);
  });

  str.on('error', function(error) {
    console.log('Error received from pushtopic: ' + error);
  });

  str.on('data', function(data) {
    console.log('Received the following from pushtopic ---');
    console.log(data);
    // emit the record to be displayed on the page
    socket.emit('record-processed', JSON.stringify(data));
  });
});


var q = 'SELECT Id, Name, CreatedDate, BillingCity FROM Account WHERE Name = "Spiffy Cleaners" LIMIT 1';

org.query({ query: q }, function(err, resp){

  if(!err && resp.records) {

    var acc = resp.records[0];
    acc.set('Name', 'Really Spiffy Cleaners');
    acc.set('Industry', 'Cleaners');

    org.update({ sobject: acc, oauth: oauth }, function(err, resp){
      if(!err) console.log('It worked!');
    });

  }
});
*/
/*
const soap = require('soap');
const url = config.topics[0].genesys.capturePoint.url;


soap.createClient(url, function(err, client) {

  client.getTaskByCaptureId({captureId: "500q000000AKQ90AAH"}, (err, result) => {
    console.log(result);
  })

  let args = {
    captureId: "500q000000AKQ90AAH",
    channel: "Email",
    category: null,
    mediaType: "Email",
    createdDateTime: "2018-03-30",
		activationDateTime: "2018-03-30",
    dueDateTime: "2018-03-30",
    expirationDateTime: "2018-03-30",
    businessValue: 100,
    priority: 100,
    processId: null,
		ext: {
      customerId: 5125,
      productType: "Loan Appointment",
      sourceProcessType: "GBANK",
      sourceTenant: "Environment"
    },
    data: {
      entry: [
        {key: "Subject", value: "Demo 2"},
        {key: "PFS_id", value: "5125"},
        {key: "SugarUser", value: "pfs"},
        {key: "demoID", value: "gbank"},
        {key: "IWD_Attr_SendEmail", value: "false"},
        {key: "sfdc_Id", value: "500q000000AKQ90AAH"},
        {key: "sfdc_Origin", value: "Phone"},
        {key: "sfdc_Status", value: "In Progress"},
        {key: "sfdc_SupportCategory", value: "Order Inquiry"},
        {key: "sfdc_Priority", value: "Normal"},
        {key: "sfdc_CreatedDate", value: "2018-03-30T11:12:15.000Z"},
        {key: "sfdc_CaseNumber", value: "46376506"},
        {key: "sfdc_Subject", value: "Demo 2"},
        {key: "MediaType", value: "workitem"}
      ]
    },
    hold: false,
    reason: "Created event type",
    actor: "EMPConnector"
  }

  client.createTask(args, (err, result) => {
    console.log(result);
  })
});
*/

loadData(function(){
  init();
})
